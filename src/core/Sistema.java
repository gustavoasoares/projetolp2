package core;

import lp2.GerenteDeUsuarios;
import lp2.Usuario;
/**
 * Classe que gerencia os usuários do sistema.
 * @author Gustavo Soares
 *
 */
public class Sistema {
	
	private GerenteDeUsuarios gerenteDeUsuarios;
	private Usuario usuarioLogado;
	public static Sistema instance = new Sistema();
	private Sistema() {
		gerenteDeUsuarios = new GerenteDeUsuarios();
	}
	
	/** Adiciona um usuário ao sistema.
	 * @param nome Nome do usuário.
	 * @param senha Senha do usuário.
	 * @param email Email do usuário. Deve ser um email válido ex.(nome@mail.com).
	 * @param confirmarSenha Mesma senha anterior.
	 * @param dica Dica de senha.
	 * @throws Exception Lança um exceção caso o usuário digite senhas diferentes.
	 */
	public void adicionaUsuario(String nome, String senha, String email, String confirmarSenha, String dica) throws Exception {
		if(!senha.equals(confirmarSenha)){
			throw new Exception("As senhas nao combinam");
		}
		Usuario usuario = new Usuario(nome, senha, email, dica);
		gerenteDeUsuarios.adicionaUsuario(usuario);
	}
	/** Efetua login no sistema.
	 * @param nome Nome do usuário.
	 * @param senha Senha do usuário.
	 * @throws Exception
	 */
	public void login(String nome, String senha) throws Exception  {
		this.usuarioLogado = gerenteDeUsuarios.login(nome,senha);
	}
	/** Retorna o usuário logado atualmente no sistema.
	 * @return Usuário logado.
	 */
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}
}
