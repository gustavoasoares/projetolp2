package ui;


import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import controller.login.EntrarAction;
import controller.login.NovoUsuarioAction;
import java.awt.Container;
import javax.swing.JPasswordField;

/**
 * Classe que cria o painel de Login do sistema
 */
public class LoginPanel extends JPanel {
	private JTextField usuarioField;
	private Container container;
	private JPasswordField passwordField;

	/**
	 * Create the panel.
	 */
	public LoginPanel(Container container) {
		this.container = container;
		setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(105, 71, 46, 14);
		add(lblUsuario);
		
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(105, 95, 46, 14);
		add(lblSenha);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new EntrarAction(this, this.container));
		btnEntrar.setBounds(115, 130, 112, 23);
		add(btnEntrar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new NovoUsuarioAction(this.container));
		btnCadastrar.setBounds(116, 164, 111, 23);
		add(btnCadastrar);
		
		usuarioField = new JTextField();
		usuarioField.setBounds(162, 68, 86, 20);
		usuarioField.setColumns(10);
		add(usuarioField);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(162, 92, 86, 20);
		add(passwordField);
		
	}
	
	public String getNome() {
		return usuarioField.getText();
	}
	
	public String getSenha() {
		return String.valueOf(passwordField.getPassword());
	}
}
