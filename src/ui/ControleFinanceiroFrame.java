package ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.CardLayout;
/**
 * Classe que inicializa a aplicação.
 * @author Gustavo Soares
*/
public class ControleFinanceiroFrame extends JFrame {

	public static final String CADASTRO_USUARIO = "CadastroUsuario";
	public static final String LOGIN = "Login";
	public static final String HOME = "Home";
	
	
	private JPanel contentPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ControleFinanceiroFrame frame = new ControleFinanceiroFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ControleFinanceiroFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		contentPane.add(new LoginPanel(contentPane),LOGIN);
		contentPane.add(new NovoUsuarioPanel(contentPane),CADASTRO_USUARIO);
		contentPane.add(new HomePanel(),HOME);
		CardLayout layout = (CardLayout) contentPane.getLayout();
		layout.show(contentPane, LOGIN);
	}

}
