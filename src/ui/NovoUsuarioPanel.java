package ui;

import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;
import controller.novousuario.CancelarAction;
import controller.novousuario.CriarNovoUsuarioAction;
/**
 * Classe que cria o painel de cadastro de usuários.
 */
public class NovoUsuarioPanel extends JPanel {

	private Container container;
	private JTextField usuarioField;
	private JTextField emailField;
	private JPasswordField senhaField;
	private JPasswordField confirmarSenhaField;
	private JTextField dicaSenhaField;

	/**
	 * Create the panel.
	 * @param contentPane 
	 */
	public NovoUsuarioPanel(Container contentPane) {
		this.container = contentPane;
		setLayout(null);
		
		JLabel tituloNovoUsurio = new JLabel("Cadastrar novo usuário");
		tituloNovoUsurio.setBounds(10, 11, 177, 14);
		add(tituloNovoUsurio);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(108, 67, 46, 14);
		add(lblUsuario);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(108, 92, 46, 14);
		add(lblEmail);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(108, 117, 46, 14);
		add(lblSenha);
		
		JLabel lblConfirmarSenha = new JLabel("Confirmar Senha");
		lblConfirmarSenha.setBounds(108, 142, 80, 14);
		add(lblConfirmarSenha);
		
		JLabel lblDicaDaSenha = new JLabel("Dica da senha");
		lblDicaDaSenha.setBounds(108, 167, 110, 14);
		add(lblDicaDaSenha);
		
		usuarioField = new JTextField();
		usuarioField.setBounds(200, 64, 137, 20);
		add(usuarioField);
		usuarioField.setColumns(10);
		
		emailField = new JTextField();
		emailField.setBounds(200, 89, 137, 20);
		add(emailField);
		emailField.setColumns(10);
		
		senhaField = new JPasswordField();
		senhaField.setBounds(200, 114, 137, 20);
		add(senhaField);
		senhaField.setColumns(10);
		
		confirmarSenhaField = new JPasswordField();
		confirmarSenhaField.setBounds(198, 139, 139, 20);
		add(confirmarSenhaField);
		confirmarSenhaField.setColumns(10);
		
		dicaSenhaField = new JTextField();
		dicaSenhaField.setBounds(200, 164, 137, 20);
		add(dicaSenhaField);
		dicaSenhaField.setColumns(10);
		
		JButton btnCriarUsuario = new JButton("Criar Usuário");
		btnCriarUsuario.setBounds(200, 217, 111, 23);
		btnCriarUsuario.addActionListener(new CriarNovoUsuarioAction(this, container));
		add(btnCriarUsuario);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new CancelarAction(container));
		btnVoltar.setBounds(108, 217, 89, 23);
		add(btnVoltar);

	}
	
	public String getUsuario() {
		return usuarioField.getText();
	}
	
	public String getEmail() {
		return emailField.getText();
	}
	
	public String getSenha() {
		return String.valueOf(senhaField.getPassword());
	}
	
	public String getConfirmarSenha() {
		return String.valueOf(confirmarSenhaField.getPassword());
	}
	
	public String getDica() {
		return dicaSenhaField.getText();
	}
	
	
	
}
