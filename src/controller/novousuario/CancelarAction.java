package controller.novousuario;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import ui.ControleFinanceiroFrame;
/**
 * 
 * @author Gustavo Soares
 *
 */
public class CancelarAction extends AbstractAction {
	private Container painelPrincipal;

	public CancelarAction(Container painelPrincipal) {
		this.painelPrincipal = painelPrincipal;
		putValue(NAME, "CancelarAction");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}
	
	// volta para a tela de login do sistema.
	public void actionPerformed(ActionEvent e) {
				CardLayout layout = (CardLayout) painelPrincipal.getLayout();
				layout.show(painelPrincipal, ControleFinanceiroFrame.LOGIN);
	}
}
