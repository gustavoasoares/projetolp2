package controller.novousuario;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import ui.ControleFinanceiroFrame;
import ui.NovoUsuarioPanel;
import core.Sistema;
/**
 * Classe que gerencia o cadastro de usuários através da interface gráfica.
 * @author Gustavo Soares
 */
public class CriarNovoUsuarioAction extends AbstractAction {
	private Container painelPrincipal;
	private NovoUsuarioPanel view;

	public CriarNovoUsuarioAction(NovoUsuarioPanel view,
			Container painelPrincipal) {
		this.painelPrincipal = painelPrincipal;
		this.view = view;
		putValue(NAME, "SwingAction");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}
	
	// após cadastrar um usuário, o método chama a tela de login do sistema.
	public void actionPerformed(ActionEvent e) {
		try {
			Sistema.instance.adicionaUsuario(view.getUsuario(), view.getSenha(),
					view.getEmail(), view.getConfirmarSenha(), view.getDica());
			CardLayout layout = (CardLayout) painelPrincipal.getLayout();
			layout.show(painelPrincipal, ControleFinanceiroFrame.LOGIN);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(view, e1.getMessage());
		}

	}
}
