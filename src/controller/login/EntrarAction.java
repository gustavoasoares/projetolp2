package controller.login;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import ui.ControleFinanceiroFrame;
import ui.LoginPanel;
import core.Sistema;
/**
 * Classe que gerencia o login do sistema através da interface gráfica.
 * @author Gustavo Soares
 *
 */
public class EntrarAction extends AbstractAction {
	private Container painelPrincipal;
	private LoginPanel view;

	public EntrarAction(LoginPanel view, Container painelPrincipal) {
		this.view = view;
		this.painelPrincipal = painelPrincipal;
		putValue(NAME, "EntrarAction");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}
	// ao efetuar login, o método chama a tela principal do sistema.
	public void actionPerformed(ActionEvent e) {
		try {
			Sistema.instance.login(view.getNome(), view.getSenha());
			CardLayout layout = (CardLayout) painelPrincipal.getLayout();
			layout.show(painelPrincipal, ControleFinanceiroFrame.HOME);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(view, e1.getMessage());
		}
	}
}
