package controller.login;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import ui.ControleFinanceiroFrame;
/**
 * 
 * @author Gustavo Soares
 *
 */
public class NovoUsuarioAction extends AbstractAction {
	private Container painelPrincipal;

	public NovoUsuarioAction(Container painelPrincipal) {
		this.painelPrincipal = painelPrincipal;
		putValue(NAME, "NovoUsuarioAction");
		putValue(SHORT_DESCRIPTION, "Some short description");
	}
	// Chama a tela de cadastro de usuários.
	public void actionPerformed(ActionEvent e) {
				CardLayout layout = (CardLayout) painelPrincipal.getLayout();
				layout.show(painelPrincipal, ControleFinanceiroFrame.CADASTRO_USUARIO);
	}
}
